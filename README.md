## Web Calculator

#### Supported:

* common arithmetic operations with integers;
* minus-unary operator.

#### Not supported*:

* operations with float-pointing numbers;
* operations with big integers;
* expressions like -(1+1)+1, when unary minus is in the first position before parentheses;
* expressions with whitespaces, letters and specials symbols except "(", ")" "+", "*" , "/" , "-".



'*' - this points weren't mentioned in the task, so I excluded them.