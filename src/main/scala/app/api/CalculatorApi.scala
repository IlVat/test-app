package app.api

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._

import app.models.ExprData
import app.services.EvaluationServiceLike
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

class CalculatorApi(val evalService: EvaluationServiceLike) extends PlayJsonSupport {
  
  import app.utils.ReadsWritesHelper.ExpressionReadsWrites.reads
  import app.utils.ReadsWritesHelper.ResultReadsWrites.writes
  import app.utils.ReadsWritesHelper.EvaluationErrorReadsWrites._
  
  lazy val route = path("evaluate") {
    post {
      entity(as[ExprData]) { expr =>
        val evaluate = evalService.evaluate(expr)
        evaluate match {
          case Right(result) => complete((OK, result))
          case Left(err) => complete((BadRequest, err))
        }
      }
    }
  }
}

object CalculatorApi {
  def apply(evalService: EvaluationServiceLike) = new CalculatorApi(evalService)
}

