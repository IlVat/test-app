package app

import com.typesafe.config.ConfigFactory

object AppConfig {
  private val config = ConfigFactory.load()
  val host =  config.getString("host")
  val port =  config.getInt("port")
}
