package app.services

import scala.util.Try

import app.models.{ExprData, Result}
import app.services.EvaluationService.errors._

trait EvaluationServiceLike {
  def evaluate(expr: ExprData): Either[EvaluationError, Result]
}

class EvaluationService extends EvaluationServiceLike {
  import app.parser.ArithmeticParser._
  
  override def evaluate(e: ExprData): Either[EvaluationError, Result] = {
    Try(expr.parse(e.expression).get) match {
      case scala.util.Success(x) => Right(Result(x.value))
      case scala.util.Failure(_: ArithmeticException) => Left(ArithmeticError)
      case scala.util.Failure(_: NumberFormatException) => Left(NumberNotIntegerError)
      case scala.util.Failure(_) => Left(ParsingError)
    }
  }
}

object EvaluationService {
  def apply(): EvaluationService = new EvaluationService()
  
  object errors {
    sealed trait EvaluationError {
      def key: String = "error"
      def valueKey: String
    }
    
    case object ArithmeticError extends EvaluationError { val valueKey = "error.divided.by.zero" }
    case object NumberNotIntegerError extends EvaluationError { val valueKey = "error.number.not.integer" }
    case object ParsingError extends EvaluationError { val valueKey = "error.wrong.expr.syntax" }
  }
}

