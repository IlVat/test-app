package app.utils

import app.models.{ExprData, Result}
import app.services.EvaluationService.errors.{ArithmeticError, EvaluationError, ParsingError}
import play.api.libs.json._
import play.api.libs.functional.syntax._

object ReadsWritesHelper {
  
  object ExpressionReadsWrites {
    
    implicit val reads: Reads[ExprData] = (__ \ 'expression).read[String].map(ExprData)
    
    implicit val writes: Writes[ExprData] =
      (__ \ 'expression).write[String].contramap { (expr: ExprData) => expr.expression }
  }
  
  object ResultReadsWrites {
    
    implicit val reads: Reads[Result] = (__ \ 'result).read[Int].map(Result)
    
    implicit val writes: Writes[Result] =
      (__ \ 'result).write[Int].contramap { (res: Result) => res.result }
  }
  
  object EvaluationErrorReadsWrites {
    implicit object EvaluationErrorWrites extends Writes[EvaluationError] {
      override def writes(o: EvaluationError): JsValue = {
        JsObject(Seq(o.key -> JsString(o.valueKey)))
      }
    }
  
    implicit object EvaluationErrorReads extends Reads[EvaluationError] {
      override def reads(json: JsValue): JsResult[EvaluationError] = {
        (json \ "error").validate[String].map {
          case ArithmeticError.valueKey => ArithmeticError
          case ParsingError.valueKey => ParsingError
        }
      }
    }
  }
}
