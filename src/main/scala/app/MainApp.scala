package app

import scala.io.StdIn
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import app.api.CalculatorApi
import app.services.EvaluationService




object MainApp extends App {
  
  import AppConfig._
  
  implicit val system = ActorSystem("web-calculator-system")
  implicit val materializer = ActorMaterializer()
  
  val evalService = EvaluationService()
  val calculator = CalculatorApi(evalService)
  
  val bindFtr = Http().bindAndHandle(calculator.route, host, port)
  
  println(s"Server online at http://$host:$port\nPress RETURN/ENTER to stop")
  
  StdIn.readLine()
  
  bindFtr
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
