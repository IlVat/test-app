package app.parser

import fastparse.all._

object ArithmeticParser {
  
  lazy val unaryMinus: P[Unit] = P("-".rep(min = 0, max = 1))
  lazy val number: P[Int] = P((unaryMinus ~ CharIn('0' to '9').rep(1)).!.map(_.toInt))
  lazy val parentheses: P[Int] = P("(" ~/ addSub ~ ")")
  lazy val factor: P[Int] = P(number | parentheses)
  lazy val divMul: P[Int] = P(factor ~ (CharIn("*/").! ~/ factor).rep).map(eval)
  lazy val addSub: P[Int] = P(divMul ~ (CharIn("+-").! ~/ divMul).rep).map(eval)
  lazy val expr: P[Int] = P(addSub ~ End)
  
  def eval(tree: (Int, Seq[(String, Int)])): Int = {
    val (base, ops) = tree
    ops.foldLeft(base) { case (left, (op, right)) =>
      op match {
        case "+" => left + right
        case "-" => left - right
        case "*" => left * right
        case "/" => left / right
      }
    }
  }
}
