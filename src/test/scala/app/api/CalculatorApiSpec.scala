package app.api

import akka.http.scaladsl.model.StatusCodes

import app.models.{ExprData, Result}
import app.services.EvaluationService
import app.services.EvaluationService.errors._

class CalculatorApiSpec extends AbstractSpec {
  
  import app.utils.ReadsWritesHelper.ExpressionReadsWrites.writes
  import app.utils.ReadsWritesHelper.ResultReadsWrites.reads
  import app.utils.ReadsWritesHelper.EvaluationErrorReadsWrites._
  
  lazy val service = EvaluationService()
  lazy val calculator = CalculatorApi(service)
  lazy val expr = ExprData("(1-1)*2+3*(1-3+4)+10/2")
  lazy val unaryExpr = ExprData("-1+1")
  lazy val dividedByZero = ExprData("1/0")
  lazy val whitespacesInExpr = ExprData("1 - 2")
  lazy val notClosedParenthesesInExpr = ExprData("(1-2*3")
  lazy val lettersInExpr = ExprData("a-2*3")
  
  "CalculatorApi evaluation" should {
    "returns 200 OK and result" in {
      Post("/evaluate", expr) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[Result].result shouldBe 11
      }
    }
  
    "returns 200 OK for unary operations" in {
      Post("/evaluate", unaryExpr) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[Result].result shouldBe 0
      }
    }
  
    "returns 400 BadRequest and ArithmeticError" in {
      Post("/evaluate", dividedByZero) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[EvaluationError] shouldBe ArithmeticError
      }
    }
  
    "returns 400 BadRequest and ParsingError for whitespaces in expression" in {
      Post("/evaluate", whitespacesInExpr) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[EvaluationError] shouldBe ParsingError
      }
    }
    
    "returns 400 BadRequest and ParsingError when parentheses wasn't close" in {
      Post("/evaluate", notClosedParenthesesInExpr) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[EvaluationError] shouldBe ParsingError
      }
    }
  
    "returns 400 BadRequest and ParsingError when letters are in expression" in {
      Post("/evaluate", lettersInExpr) ~> calculator.route ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[EvaluationError] shouldBe ParsingError
      }
    }
  }
}
