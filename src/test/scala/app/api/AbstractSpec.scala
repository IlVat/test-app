package app.api

import akka.http.scaladsl.testkit.ScalatestRouteTest

import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import org.scalatest.{Matchers, WordSpec}

trait AbstractSpec extends WordSpec with Matchers with ScalatestRouteTest with PlayJsonSupport