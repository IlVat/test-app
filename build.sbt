name := "test-app"

version := "1.0"

scalaVersion := "2.12.3"

val akkaHttpVer = "10.0.9"

libraryDependencies ++= Seq(
  "com.typesafe.akka"   %% "akka-http"            % akkaHttpVer,
  "de.heikoseeberger"   %% "akka-http-play-json"  % "1.15.0",
  "com.lihaoyi"         %% "fastparse"            % "0.4.3",
  "com.typesafe.akka"   %% "akka-http-testkit"    % akkaHttpVer   % "test",
  "org.scalatest"       %% "scalatest"            % "3.0.1"       % "test"
)


        